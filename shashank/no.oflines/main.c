#include <stdio.h>

int main() {
    FILE *file;
    char filename[100];
    int lineCount = 0;
    char ch;

    printf("Enter the file name: ");
    scanf("%s", filename);

    // Open file in read mode
    file = fopen(filename, "r");
    
    if (file == NULL) {
        printf("Unable to open the file.\n");
        return 1;
    }

    // Count lines
    while ((ch = fgetc(file)) != EOF) {
        if (ch == '\n') {
            lineCount++;
        }
    }
 
    printf("Total lines: %d\n", lineCount);

    // Close the file
    fclose(file);

    return 0;
}

