#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#define MAX_EMPLOYEES 100

typedef struct {
    int id;
    char name[50];
    float salary;
} Employee;

void addEmployee(Employee employees[], int *count);
void deleteEmployee(Employee employees[], int *count);
void displayEmployees(Employee employees[], int count);

#endif  /* EMPLOYEE_H */

