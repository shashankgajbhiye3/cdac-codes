#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include "employee.h"

void addEmployee(Employee employees[], int *count) {
    if (*count >= MAX_EMPLOYEES) {
        printf("Maximum number of employees reached.\n");
        return;
    }

    Employee newEmployee;
    printf("Enter employee ID: ");
    scanf("%d", &newEmployee.id);
    printf("Enter employee name: ");
    scanf("%s", newEmployee.name);
    printf("Enter employee salary: ");
    scanf("%f", &newEmployee.salary);

    employees[*count] = newEmployee;
    (*count)++;

    printf("Employee added successfully.\n");
}

void deleteEmployee(Employee employees[], int *count) {
    if (*count == 0) {
        printf("No employees to delete.\n");
        return;
    }

    int employeeID;
    printf("Enter employee ID to delete: ");
    scanf("%d", &employeeID);

    int found = 0;
    for (int i = 0; i < *count; i++) {
        if (employees[i].id == employeeID) {
            found = 1;
            for (int j = i; j < *count - 1; j++) {
                employees[j] = employees[j + 1];
            }
            (*count)--;
            printf("Employee deleted successfully.\n");
            break;
        }
    }

    if (!found) {
        printf("Employee not found.\n");
    }
}

void displayEmployees(Employee employees[], int count) {
    if (count == 0) {
        printf("No employees to display.\n");
        return;
    }

    printf("Employee List:\n");
    for (int i = 0; i < count; i++) {
        printf("ID: %d, Name: %s, Salary: %.2f\n",
               employees[i].id, employees[i].name, employees[i].salary);
    }
}

void saveEmployeesToFile(Employee employees[], int count, const char* filename) {
    int file = open(filename, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    if (file == -1) {
        printf("Error opening file.\n");
        return;
    }

    char buffer[256];

    for (int i = 0; i < count; i++) {
        sprintf(buffer, "%d,%s,%.2f\n", employees[i].id, employees[i].name, employees[i].salary);
        write(file, buffer, strlen(buffer));
    }

    close(file);
    printf("Employees saved to file successfully.\n");
}

void loadEmployeesFromFile(Employee employees[], int* count, const char* filename) {
    int file = open(filename, O_RDONLY);
    if (file == -1) {
        printf("Error opening file.\n");
        return;
    }

    char buffer[256];
    int bytesRead = 0;
    int index = 0;

    while ((bytesRead = read(file, buffer, sizeof(buffer))) > 0) {
        int start = 0;

        for (int i = 0; i < bytesRead; i++) {
            if (buffer[i] == '\n') {
                buffer[i] = '\0';

                char* token = strtok(&buffer[start], ",");
                employees[index].id = atoi(token);

                token = strtok(NULL, ",");
                strcpy(employees[index].name, token);

                token = strtok(NULL, ",");
                employees[index].salary = atof(token);

                index++;
                start = i + 1;
            }
        }
    }

    *count = index;

    close(file);
    printf("Employees loaded from file successfully.\n");
}

