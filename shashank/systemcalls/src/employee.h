#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#define MAX_NAME_LENGTH 50
#define MAX_EMPLOYEES 100

typedef struct {
    int id;
    char name[MAX_NAME_LENGTH];
    float salary;
} Employee;

void addEmployee(Employee employees[], int *count);
void deleteEmployee(Employee employees[], int *count);
void displayEmployees(Employee employees[], int count);
void saveEmployeesToFile(Employee employees[], int count, const char* filename);
void loadEmployeesFromFile(Employee employees[], int* count, const char* filename);

#endif  // EMPLOYEE_H

