#include <stdio.h>
#include "employee.h"

#define MAX_EMPLOYEES 100

int main() {
    Employee employees[MAX_EMPLOYEES];
    int count = 0;
    char filename[] = "employees.txt";

    loadEmployeesFromFile(employees, &count, filename);

    int choice;
    do {
        printf("\nEmployee Management System\n");
        printf("1. Add Employee\n");
        printf("2. Delete Employee\n");
        printf("3. Display Employees\n");
        printf("4. Save Employees to File\n");
        printf("5. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                addEmployee(employees, &count);
                break;
            case 2:
                deleteEmployee(employees, &count);
                break;
            case 3:
                displayEmployees(employees, count);
                break;
            case 4:
                saveEmployeesToFile(employees, count, filename);
                break;
            case 5:
                printf("Exiting...\n");
                break;
            default:
                printf("Invalid choice. Try again.\n");
                break;
        }
    } while (choice != 5);

    return 0;
}

