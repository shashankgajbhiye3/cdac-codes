#include <stdio.h>

int main() {
    FILE *file;
    char ch;
    int vowels = 0, consonants = 0, spaces = 0, tabs = 0, specials = 0, lines = 0;

    // Open the file in read mode
    file = fopen("filename.txt", "r");

    // Check if the file opened successfully
    if (file == NULL) {
        printf("File could not be opened.\n");
        return 1;
    }

    // Read the file character by character
    while ((ch = fgetc(file)) != EOF) {
        if (ch == '\n') {
            lines++;
        } else if (ch == ' '){
            spaces++;
        } else if (ch == '\t'){
            tabs++;
        } else if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')) {
            ch = tolower(ch); // convert to lowercase
            if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
                vowels++;
            } else {
                consonants++;
            }
        } else {
            specials++;
        }
    }

    // Close the file
    fclose(file);

    // Print the results
    printf("Vowels: %d\n", vowels);
    printf("Consonants: %d\n", consonants);
    printf("Spaces: %d\n", spaces);
    printf("Tabs: %d\n", tabs);
    printf("Special symbols: %d\n", specials);
    printf("Lines: %d\n", lines);

    return 0;
}

